import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.TableModel;
public class loginContext
{
    Connection con;
    PreparedStatement pst;
    ResultSet rs;
   
    public loginContext()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/finalproject", "root", "");
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
    }
    
    
    public boolean checkLogin(String email,String pass)
    {
        try 
        {      
        	pst=con.prepareStatement("select email,pass from singup where email=? and pass=?");
            pst.setString(1, email); //this replaces the 1st  "?" in the query for username
            pst.setString(2, pass);    //this replaces the 2st  "?" in the query for password
            rs=pst.executeQuery();
            if(rs.next())
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        catch (Exception ex) 
        {
            System.out.println("Error while validating"+ex);
            return false;
        }
    }
    
    public boolean getUs(String email)
	{
		try 
		{
			pst=con.prepareStatement("SELECT email FROM singup where email=?");
			pst.setString(1, email);
			rs=pst.executeQuery();
			if(rs.next())
            {
                return true;
            }
            else
            {
                return false;
            }
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return false;
		}
	}
    
    public boolean getCheckSerch(String dname)
	{
		try 
		{
			pst=con.prepareStatement("SELECT dname FROM admin where dname = ?");
			pst.setString(1, dname);
			rs=pst.executeQuery();
			if(rs.next())
            {
                return false;
            }
            else
            {
                return true;
            }
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return false;
		}
	}
}

