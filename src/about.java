import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class about extends JFrame implements ActionListener{

	private JPanel panel,panel1,panel2,panel3;
	private JLabel labelImg,labelW,labelA,labelN,labelD,labelE,labelR,labelBd,labelTit1,labelTit2,labelTit3,labelTit4,labelTit5;
	private JLabel labelK1,labelk2,labelK3,labelK4,labelK5,labelK6,labelK7;
	private JButton buttonBack;
	private JScrollBar scroll;
	
	public about()
	{
		this.AbFunc();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Back"))
		{
			serchEngin se = new serchEngin();
			se.setVisible(true);
			this.setVisible(false);
		}
	}
	
	public void AbFunc()
	{
		this.panel = new JPanel();
		this.panel.setBackground(Color.black);
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(0x00000A));
		this.panel1.setSize(1366, 65);
		this.panel1.setLocation(0, 0);
		this.panel1.setLayout(null);
		
		/*this.panel2 = new JPanel();
		this.panel2.setBackground(new Color(0x080800));
		this.panel2.setSize(400, 560);
		this.panel2.setLocation(880, 65);
		this.panel2.setLayout(null);*/
		
		this.panel3 = new JPanel();
		this.panel3.setBackground(new Color(0x00000A));
		this.panel3.setSize(1366, 65);
		this.panel3.setLocation(0, 625);
		this.panel3.setLayout(null);
		
		this.labelImg = new JLabel();
		this.labelImg.setIcon(new ImageIcon("C:\\Users\\PRABAL MALLICK\\Desktop\\code prac\\FinalProject_WanderBd\\Image\\about.PNG"));
		this.labelImg.setSize(400, 350);
		this.labelImg.setLocation(870, 160);
		this.panel.add(this.labelImg);
		
		this.labelW = new JLabel("W");
		this.labelW.setBounds(210,8, 600, 50);
		this.labelW.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelW.setForeground(Color.BLUE);
		this.panel.add(this.labelW);
		
		this.labelA = new JLabel("a");
		this.labelA.setBounds(250, 18, 100, 30);
		this.labelA.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelA.setForeground(Color.RED);
		this.panel.add(this.labelA);
		
		this.labelN = new JLabel("n");
		this.labelN.setBounds(268, 18, 100, 30);
		this.labelN.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelN.setForeground(Color.GREEN);
		this.panel.add(this.labelN);
		
		this.labelD = new JLabel("d");
		this.labelD.setBounds(287, 19, 100, 30);
		this.labelD.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelD.setForeground(Color.DARK_GRAY);
		this.panel.add(this.labelD);
		
		this.labelE = new JLabel("e");
		this.labelE.setBounds(307, 18, 100, 30);
		this.labelE.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelE.setForeground(Color.orange);
		this.panel.add(this.labelE);
		
		this.labelR = new JLabel("R");
		this.labelR.setBounds(327, 18, 100, 30);
		this.labelR.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelR.setForeground(Color.BLUE);
		this.panel.add(this.labelR);
		
		this.labelBd = new JLabel(" BD");
		this.labelBd.setBounds(357, 30, 100, 30);
		this.labelBd.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,30));
		this.labelBd.setForeground(new Color(0xCCCCA3));
		this.panel.add(this.labelBd);
		
		this.labelTit1 = new JLabel("Welcome To Wander Bangladesh");
		this.labelTit1.setBounds(40, 110, 360, 35);
		this.labelTit1.setFont(new Font("Freestyle Script",Font.BOLD,34));
		this.labelTit1.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit1);
		
		this.labelTit2 = new JLabel("This is a Offline Search Engine Which Will Give You Information About 64 Districts in Bangladesh .");
		this.labelTit2.setBounds(40, 170, 800, 30);
		this.labelTit2.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelTit2.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit2);
		
		this.labelTit3 = new JLabel("You Can Register to This App And After Register You Will Able to Update Districts Wise Information");
		this.labelTit3.setBounds(40, 190, 800, 30);
		this.labelTit3.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelTit3.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit3);
		
		this.labelTit4 = new JLabel("You Would Like.So Increase Your Knowledge About Bangladesh And Stay Conected With Wander Bd.");
		this.labelTit4.setBounds(40, 210, 800, 30);
		this.labelTit4.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelTit4.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit4);
		
		this.labelTit5 = new JLabel("Keywords");
		this.labelTit5.setBounds(40, 270, 360, 35);
		this.labelTit5.setFont(new Font("Freestyle Script",Font.BOLD,34));
		this.labelTit5.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit5);
		
		this.labelK1 = new JLabel("Dhaka,Gazipur,Narayanganj,Narsingdi,Munshiganj,Manikganj,Tangail,Mymensingh,Jamalpur,Sherpur,");
		this.labelK1.setBounds(40, 325, 800, 30);
		this.labelK1.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelK1.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelK1);
		
		this.labelk2 = new JLabel("Netrakona,Kishoreganj,Shariatpur,Madaripur,Gopalganj,Faridpur,Rajbari,Chittagong,Coxs Bazar,Bhola,");
		this.labelk2.setBounds(40, 345, 800, 30);
		this.labelk2.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelk2.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelk2);
		
		this.labelK3 = new JLabel("Bandarban,Rangamati,Khagrachhari,Feni,Noakhali,Lakshmipur,Chandpur,Comilla,Brahmanbaria,Rajshahi,");
		this.labelK3.setBounds(40, 365, 800, 30);
		this.labelK3.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelK3.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelK3);
		
		this.labelK4 = new JLabel("Chapainawabganj,Naogaon,Natore,Pabna,Sirajganj,Bogra,Bogra,Rangpur,Kurigram,Gaibandha,Lalmonirhat,");
		this.labelK4.setBounds(40, 385, 800, 30);
		this.labelK4.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelK4.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelK4);
		
		this.labelK5 = new JLabel("Nilphamari,Dinajpur,Thakurgaon,Panchagarh,Khulna,Satkhira,Bagerhat,Narail,Jessore,Jhenaidah,Magura,");
		this.labelK5.setBounds(40, 405, 800, 30);
		this.labelK5.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelK5.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelK5);
		
		this.labelK6 = new JLabel("Chuadanga,Kushtia,Meherpur,Barisal,Jhalokati,Pirojpur,Barguna,Patuakhali,Sylhet,Moulvibazar,Habiganj,");
		this.labelK6.setBounds(40, 425, 800, 30);
		this.labelK6.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelK6.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelK6);
		
		this.labelK7 = new JLabel("Sunamganj.");
		this.labelK7.setBounds(40, 445, 800, 30);
		this.labelK7.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelK7.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelK7);
		
		this.scroll = new JScrollBar();
		this.setBounds(0, 0, 1100, 900);
		this.panel.add(this.scroll);
		
		this.buttonBack = new JButton("Back");
		this.buttonBack.setBounds(610, 642, 180, 30);
		this.buttonBack.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonBack.setBackground(new Color(0x00000A));
		this.buttonBack.setForeground(new Color(0xF2E6E6));
		this.buttonBack.addActionListener(this);
		this.panel.add(this.buttonBack);
		
		this.add(this.panel);
		this.panel.add(this.panel1);
		//this.panel.add(this.panel2);
		this.panel.add(this.panel3);
		
		this.setSize(1366, 727);
		this.setTitle("Profile");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
