import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class singUp extends JFrame implements ActionListener{
	
	private JPanel panel,panel1,panel2;
	private JLabel labelImg,labelTit,labelTit1,labelInfo,labelTit2,labelName,labelGender,labelEmail,labelPass;
	private JTextField textName,textEmail;
	private JPasswordField textPass;
	private JRadioButton radioMale,radioFemale;
	private JButton buttonSingup;
	private ButtonGroup group;
	
	public singUp()
	{
		this.setup();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Sing Up"))
		{
			loginContext lCon = new loginContext();
			String name = textName.getText().trim();
			String email = textEmail.getText().trim();
			String pass = textPass.getText().trim();
			if(name.equals(""))
			{
				JOptionPane.showMessageDialog(null, "Enter Your Name !");
			}
			else if(email.equals(""))
			{
				JOptionPane.showMessageDialog(null, "Enter Your Email As Username !");
			}
			else if(lCon.getUs(email))
			{
				JOptionPane.showMessageDialog(null, "Email Address Already Exist Try Another !");
			}
			else if(pass.equals(""))
			{
				JOptionPane.showMessageDialog(null, "Set Your Password !");
			}
			else
			{
				data2 dat2 = new data2();
				dat2.setFname(name);
				String gender = "";
					if(this.radioMale.isSelected() == true)
		            {
		                gender = "Male";
		            }
					else if(this.radioFemale.isSelected() == true)
		            {
		                gender = "Female";
		            }
					
				dat2.setGender(gender);
				dat2.setEmail(email);
				
				dat2.setPass(pass);
				context con = new context();
				con.Save(dat2);
				JOptionPane.showMessageDialog(null, "Registration Successfully Done");
				
				textName.setText("");
				group.clearSelection();
				textEmail.setText("");
				textPass.setText("");

				int response = JOptionPane.showConfirmDialog(null, "Back to Home", "Confirm",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.NO_OPTION) 
				{
					this.setVisible(true);
				} 
				else if (response == JOptionPane.YES_OPTION) 
				{
					serchEngin se = new serchEngin();
					se.setVisible(true);
					this.setVisible(false);
				}
			}
		}
	}
	
	public void setup()
	{
		this.panel = new JPanel();
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(0x0C090A));
		this.panel1.setSize(700, 727);
		this.panel1.setLocation(700, 0);
		this.panel1.setLayout(null);
		
		
		this.labelImg = new JLabel();
		this.labelImg.setIcon(new ImageIcon("C:\\Users\\PRABAL MALLICK\\Desktop\\code prac\\FinalProject_WanderBd\\Image\\visit.PNG"));
		this.labelImg.setSize(950, 727);
		this.panel.add(this.labelImg);
		
		this.labelTit = new JLabel("Wolcome to Wander BD");
		this.labelTit.setBounds(800, 50, 600, 50);
		this.labelTit.setFont(new Font("Andalus",Font.BOLD,40));
		this.labelTit.setForeground(new Color(0xF2F2F2));
		this.panel.add(this.labelTit);
		
		this.labelTit1 = new JLabel("Discover Bangladesh and Stay Connectet With Wander");
		this.labelTit1.setBounds(805, 90, 600, 50);
		this.labelTit1.setFont(new Font("Times New Roman",Font.PLAIN,20));
		this.labelTit1.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit1);
		
		this.labelTit2 = new JLabel("Bangladesh...");
		this.labelTit2.setBounds(805, 110, 600, 50);
		this.labelTit2.setFont(new Font("Times New Roman",Font.PLAIN,20));
		this.labelTit2.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit2);
		
		this.labelName = new JLabel("Full Name");
		this.labelName.setBounds(808, 230, 100, 25);
		this.labelName.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelName.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelName);
		
		this.textName = new JTextField();
		this.textName.setBounds(940, 230, 260, 25);
		this.textName.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textName);
		
		this.labelGender = new JLabel("Gender");
		this.labelGender.setBounds(808, 280, 100, 25);
		this.labelGender.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelGender.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelGender);
		
		this.radioMale = new JRadioButton("Male");
		this.radioMale.setBounds(935, 285, 80, 20);
		this.radioMale.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.radioMale.setBackground(new Color(0x0C090A));
		this.radioMale.setForeground(new Color(0xffffc1));
		this.panel.add(this.radioMale);
		
		this.radioFemale = new JRadioButton("Female");
		this.radioFemale.setBounds(935, 310, 100, 20);
		this.radioFemale.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.radioFemale.setBackground(new Color(0x0C090A));
		this.radioFemale.setForeground(new Color(0xffffc1));
		this.panel.add(this.radioFemale);
		
		this.group = new ButtonGroup();
		this.group.add(this.radioFemale);
		this.group.add(this.radioMale);
		
		this.labelEmail = new JLabel("Email Address");
		this.labelEmail.setBounds(808, 360, 130, 25);
		this.labelEmail.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelEmail.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelEmail);
		
		this.textEmail = new JTextField();
		this.textEmail.setBounds(940, 360, 260, 25);
		this.textEmail.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textEmail);
		
		this.labelPass = new JLabel("Password");
		this.labelPass.setBounds(808, 410, 130, 25);
		this.labelPass.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelPass.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPass);
		
		this.textPass = new JPasswordField();
		this.textPass.setBounds(940, 410, 260, 25);
		this.textPass.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,20));
		this.panel.add(this.textPass);
		
		this.labelInfo = new JLabel("*At Least 6 Digit");
		this.labelInfo.setBounds(940, 435, 130, 25);
		this.labelInfo.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,10));
		this.labelInfo.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelInfo);
		
		this.buttonSingup = new JButton("Sing Up");
		this.buttonSingup.setBounds(910, 530, 220, 35);
		this.buttonSingup.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonSingup.setBackground(new Color(0x260000));
		this.buttonSingup.setForeground(new Color(0xF2E6E6));
		this.buttonSingup.addActionListener(this);
		this.panel.add(this.buttonSingup);
	
		this.add(this.panel);
		this.panel.add(this.panel1);
		
		
		//this.pack();
		this.setSize(1366, 727);
		this.setTitle("Sing Up");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		validate();
	}
}
