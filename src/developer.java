import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class developer extends JFrame implements ActionListener{
	
	private JPanel panel,panel1,panel2,panel3,panel4,panel5,panel6,panel7;
	private JLabel labelW,labelA,labelN,labelD,labelE,labelR,labelBd;
	private JLabel labelPname,labelPid,labelPuv,labelPdep,labelPemail,labelPimage,labelins;
	private JLabel labelAvname,labelAvid,labelAvuv,labelAVdep,labelAvemail,labelAvimage;
	private JLabel labelAmname,labelAmid,labelAmuv,labelAmdep,labelAmemail,labelAmimage;
	private JLabel labelSirname,labelSiruv,labelSirdep,labelSiremail;
	private JLabel labelGroup,labelPronam,labelSub,labelStdate,labelEdate,labelSbdate;
	private JButton buttonBack;

	public developer()
	{
		this.devTools();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Back"))
		{
			serchEngin se = new serchEngin();
			se.setVisible(true);
			this.setVisible(false);
		}
	}
	
	public void devTools()
	{
		this.panel = new JPanel();
		this.panel.setBackground(Color.black);
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(0x00000A));
		this.panel1.setSize(1366, 65);
		this.panel1.setLocation(0, 0);
		this.panel1.setLayout(null);
		
		this.panel2 = new JPanel();
		this.panel2.setBackground(new Color(0x080800));
		this.panel2.setSize(600, 560);
		this.panel2.setLocation(60, 65);
		this.panel2.setLayout(null);
		
		this.panel3 = new JPanel();
		this.panel3.setBackground(new Color(0x00000A));
		this.panel3.setSize(1366, 65);
		this.panel3.setLocation(0, 625);
		this.panel3.setLayout(null);
		
		this.panel6 = new JPanel();
		this.panel6.setBackground(new Color(0x080800));
		this.panel6.setSize(460, 505);
		this.panel6.setLocation(780, 90);
		this.panel6.setLayout(null);
		
		this.labelW = new JLabel("W");
		this.labelW.setBounds(210,8, 600, 50);
		this.labelW.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelW.setForeground(Color.BLUE);
		this.panel.add(this.labelW);
		
		this.labelA = new JLabel("a");
		this.labelA.setBounds(250, 18, 100, 30);
		this.labelA.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelA.setForeground(Color.RED);
		this.panel.add(this.labelA);
		
		this.labelN = new JLabel("n");
		this.labelN.setBounds(268, 18, 100, 30);
		this.labelN.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelN.setForeground(Color.GREEN);
		this.panel.add(this.labelN);
		
		this.labelD = new JLabel("d");
		this.labelD.setBounds(287, 19, 100, 30);
		this.labelD.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelD.setForeground(Color.DARK_GRAY);
		this.panel.add(this.labelD);
		
		this.labelE = new JLabel("e");
		this.labelE.setBounds(307, 18, 100, 30);
		this.labelE.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelE.setForeground(Color.orange);
		this.panel.add(this.labelE);
		
		this.labelR = new JLabel("R");
		this.labelR.setBounds(327, 18, 100, 30);
		this.labelR.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelR.setForeground(Color.BLUE);
		this.panel.add(this.labelR);
		
		this.labelBd = new JLabel(" BD");
		this.labelBd.setBounds(357, 30, 100, 30);
		this.labelBd.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,30));
		this.labelBd.setForeground(new Color(0xCCCCA3));
		this.panel.add(this.labelBd);
		
		this.buttonBack = new JButton("Back");
		this.buttonBack.setBounds(610, 642, 180, 30);
		this.buttonBack.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonBack.setBackground(new Color(0x00000A));
		this.buttonBack.setForeground(new Color(0xF2E6E6));
		this.buttonBack.addActionListener(this);
		this.panel.add(this.buttonBack);
		
		////////////For Personal Info///////////////
		
		this.labelPname = new JLabel("Prabal Mallick");
		this.labelPname.setBounds(120, 100, 200, 25);
		this.labelPname.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelPname.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPname);
		
		this.labelPid = new JLabel("13-23802-1");
		this.labelPid.setBounds(120, 120, 200, 25);
		this.labelPid.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelPid.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPid);
		
		this.labelPuv = new JLabel("AIUB");
		this.labelPuv.setBounds(120, 140, 200, 25);
		this.labelPuv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelPuv.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPuv);
		
		this.labelPdep = new JLabel("B.Sc in CS");
		this.labelPdep.setBounds(120, 160, 200, 25);
		this.labelPdep.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelPdep.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPdep);
		
		this.labelPemail = new JLabel("prabalmallick220@yahoo.com");
		this.labelPemail.setBounds(120, 180, 240, 25);
		this.labelPemail.setFont(new Font("Arial Rounded MT Bold",Font.ROMAN_BASELINE,14));
		this.labelPemail.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPemail);
		
		this.labelPimage = new JLabel();
		this.labelPimage.setIcon(new ImageIcon("C:\\Users\\PRABAL MALLICK\\Desktop\\code prac\\FinalProject_WanderBd\\Image\\pr.PNG"));
		this.labelPimage.setSize(120, 140);
		this.labelPimage.setLocation(360, 20);
		this.panel2.add(this.labelPimage);
		
		this.panel4 = new JPanel();
		this.panel4.setBackground(new Color(0x4D4D4D));
		this.panel4.setSize(490, 2);
		this.panel4.setLocation(55, 180);
		this.panel4.setLayout(null);
		
		
		this.labelAvname = new JLabel("Avishek Kar Dip");
		this.labelAvname.setBounds(120, 280, 200, 25);
		this.labelAvname.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAvname.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAvname);
		
		this.labelAvid = new JLabel("13-23726-1");
		this.labelAvid.setBounds(120, 300, 200, 25);
		this.labelAvid.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAvid.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAvid);
		
		this.labelAvuv = new JLabel("AIUB");
		this.labelAvuv.setBounds(120, 320, 200, 25);
		this.labelAvuv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAvuv.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAvuv);
		
		this.labelAVdep = new JLabel("B.Sc in CS");
		this.labelAVdep.setBounds(120, 340, 200, 25);
		this.labelAVdep.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAVdep.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAVdep);
		
		this.labelAvemail = new JLabel("karavishek99@gmail.com");
		this.labelAvemail.setBounds(120, 360, 240, 25);
		this.labelAvemail.setFont(new Font("Arial Rounded MT Bold",Font.ROMAN_BASELINE,14));
		this.labelAvemail.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAvemail);
		
		this.labelAvimage = new JLabel();
		this.labelAvimage.setIcon(new ImageIcon("C:\\Users\\PRABAL MALLICK\\Desktop\\code prac\\FinalProject_WanderBd\\Image\\av.PNG"));
		this.labelAvimage.setSize(120, 140);
		this.labelAvimage.setLocation(360, 210);
		this.panel2.add(this.labelAvimage);
		
		this.panel5 = new JPanel();
		this.panel5.setBackground(new Color(0x4D4D4D));
		this.panel5.setSize(490, 2);
		this.panel5.setLocation(55, 370);
		this.panel5.setLayout(null);
		
		this.labelAmname = new JLabel("MD. Aminul Islam");
		this.labelAmname.setBounds(120, 470, 200, 25);
		this.labelAmname.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAmname.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAmname);
		
		this.labelAmid = new JLabel("13-23840-1");
		this.labelAmid.setBounds(120, 490, 200, 25);
		this.labelAmid.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAmid.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAmid);
		
		this.labelAmuv = new JLabel("AIUB");
		this.labelAmuv.setBounds(120, 510, 200, 25);
		this.labelAmuv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAmuv.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAmuv);
		
		this.labelAmdep = new JLabel("B.Sc in CSSE");
		this.labelAmdep.setBounds(120, 530, 200, 25);
		this.labelAmdep.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelAmdep.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAmdep);
		
		this.labelAmemail = new JLabel("aminul.aiub23840@gmail.com");
		this.labelAmemail.setBounds(120, 550, 240, 25);
		this.labelAmemail.setFont(new Font("Arial Rounded MT Bold",Font.ROMAN_BASELINE,14));
		this.labelAmemail.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelAmemail);
		
		this.labelAmimage = new JLabel();
		this.labelAmimage.setIcon(new ImageIcon("C:\\Users\\PRABAL MALLICK\\Desktop\\code prac\\FinalProject_WanderBd\\Image\\am.PNG"));
		this.labelAmimage.setSize(120, 140);
		this.labelAmimage.setLocation(360, 400);
		this.panel2.add(this.labelAmimage);
		
		this.panel7 = new JPanel();
		this.panel7.setBackground(new Color(0x4D4D4D));
		this.panel7.setSize(380, 2);
		this.panel7.setLocation(820, 250);
		this.panel7.setLayout(null);
		this.panel.add(this.panel7);
		
		this.labelins = new JLabel("Instructed by");
		this.labelins.setBounds(950, 100, 200, 25);
		this.labelins.setFont(new Font("Arial Rounded MT Bold",Font.LAYOUT_NO_START_CONTEXT,18));
		this.labelins.setForeground(new Color(0x194719));
		this.panel.add(this.labelins);
		
		this.labelSirname = new JLabel("MD. Al Imran");
		this.labelSirname.setBounds(965, 140, 200, 25);
		this.labelSirname.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelSirname.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelSirname);

		this.labelSirdep = new JLabel("Faculty(CSE)");
		this.labelSirdep.setBounds(964, 160, 200, 25);
		this.labelSirdep.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelSirdep.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelSirdep);
		
		
		this.labelSiruv = new JLabel("AIUB");
		this.labelSiruv.setBounds(987, 180, 200, 25);
		this.labelSiruv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelSiruv.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelSiruv);
		
		
		this.labelSiremail = new JLabel("alimran@aiub.edu");
		this.labelSiremail.setBounds(952, 200, 200, 25);
		this.labelSiremail.setFont(new Font("Arial Rounded MT Bold",Font.ROMAN_BASELINE,14));
		this.labelSiremail.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelSiremail);
		
		
		this.labelPronam = new JLabel("Wander Bangladesh");
		this.labelPronam.setBounds(820, 300, 200, 25);
		this.labelPronam.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,20));
		this.labelPronam.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPronam);
		
		this.labelGroup = new JLabel("Group Project");
		this.labelGroup.setBounds(820, 360, 200, 25);
		this.labelGroup.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelGroup.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelGroup);
		
		this.labelSub = new JLabel("OOP1(Java)");
		this.labelSub.setBounds(820, 380, 200, 25);
		this.labelSub.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelSub.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelSub);
		
		this.labelStdate = new JLabel("Starting Date: 16.12.2014");
		this.labelStdate.setBounds(820, 400, 200, 25);
		this.labelStdate.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelStdate.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelStdate);
		
		this.labelEdate = new JLabel("Fenishing Date: 20.12.2014");
		this.labelEdate.setBounds(820, 420, 200, 25);
		this.labelEdate.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelEdate.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelEdate);
		
		this.labelSbdate = new JLabel("Submition Date: 22.12.2014");
		this.labelSbdate.setBounds(820, 440, 200, 25);
		this.labelSbdate.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,14));
		this.labelSbdate.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelSbdate);
		
		
		this.add(this.panel);
		this.panel.add(this.panel1);
		this.panel.add(this.panel2);
		this.panel.add(this.panel3);
		this.panel.add(this.panel6);
		this.panel2.add(this.panel4);
		this.panel2.add(this.panel5);
		
		this.setSize(1366, 727);
		this.setTitle("Profile");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
