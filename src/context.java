import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;

import javax.print.attribute.TextSyntax;

public class context {

	private Connection connection;
	private Statement statement;
	
	public context()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/finalproject", "root", "");
			this.statement = this.connection.createStatement();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	
	//For publicSearch
	public Vector<Vector<String>> getpublicSearchAsString(data1 dat1)
	{
		try
		{
			String sql = "SELECT * FROM admin where dname like '%"+dat1.getSearch()+"%'";
			ResultSet rs = this.statement.executeQuery(sql);
			Vector<Vector<String>> serch = new Vector<Vector<String>>();
			while(rs.next())
			{
				Vector<String> PublicSrc = new Vector<String>();
				PublicSrc.add(rs.getString("dname"));
				PublicSrc.add(rs.getString("establish"));
				PublicSrc.add(rs.getString("division"));
				PublicSrc.add(rs.getString("area"));
				PublicSrc.add(rs.getString("population"));
				PublicSrc.add(rs.getString("litarecy"));
				PublicSrc.add(rs.getString("place"));
				serch.add(PublicSrc);
			}
			return serch;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public Vector<Vector<String>> getdataListAsString()
	{
		try
		{
			String sql = "SELECT * FROM public";
			ResultSet rs = this.statement.executeQuery(sql);
			Vector<Vector<String>> showSelf = new Vector<Vector<String>>();
			while(rs.next())
			{
				Vector<String> self = new Vector<String>();
				self.add(rs.getString("dname"));
				self.add(rs.getString("establish"));
				self.add(rs.getString("division"));
				self.add(rs.getString("area"));
				self.add(rs.getString("population"));
				self.add(rs.getString("litarecy"));
				self.add(rs.getString("place"));
				showSelf.add(self);
			}
			return showSelf;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public boolean Save(data2 dat2)
	{
		try
		{
			String sql = "INSERT INTO singup VALUES (null, '" + dat2.getFname() +  "', '" + dat2.getGender() +  "', '" + dat2.getEmail() + "', '" + dat2.getPass() +  "')";
			this.statement.execute(sql);
			return true;
		}
		catch(Exception ex)
		{
			System.out.print(ex);
			return false;
		}		
	}
	
	public boolean Save(data1 dat1)
	{
		try
		{
			String sql = "INSERT INTO public VALUES ('" + dat1.getDistName() +  "', '" + dat1.getEstablish() +  "', '" + dat1.getDivision() + "', '" + dat1.getArea() + "', '" + dat1.getPopulation() + "', '" + dat1.getLitarecy() + "',  '" + dat1.getHistorical() + "')";
			this.statement.execute(sql);
			return true;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}		
	}
}

