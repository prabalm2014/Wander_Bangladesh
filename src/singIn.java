import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class singIn extends JFrame implements ActionListener{
	
	private JPanel panel,panel1;
	private JLabel labelImg,labelTit,labelTit1,labelTit2,labelTit3,labelTit4,labelUser,labelPass;
	private JTextField textUser;
	private JPasswordField textPass;
	private JButton buttonSingin,buttonHere;



	public singIn()
	{
		this.component();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Sign In"))
		{
			loginContext lCon = new loginContext();
			char[] temp_pwd=textPass.getPassword();
            String pwd=null;
            pwd=String.copyValueOf(temp_pwd);
            
            if(lCon.checkLogin(textUser.getText(), pwd))
            {
            	JOptionPane.showMessageDialog(null, "You have Logged in Successfully");
                                      
            	afterLogin al = new afterLogin();
        		al.setVisible(true);
	    		this.setVisible(false);
            }
            else
            {
            	JOptionPane.showMessageDialog(null, "Invalid Username and Password!");
            	textPass.setText("");
            }
		}
		else if(buttPress.equals("Here"))
		{
			singUp su = new singUp();
			su.setVisible(true);
			this.setVisible(false);
		}
	}
	
	public void component()
	{
		this.panel = new JPanel();
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(0x0C090A));
		this.panel1.setSize(700, 727);
		this.panel1.setLocation(800, 0);
		this.panel1.setLayout(null);
		
		this.labelImg = new JLabel();
		this.labelImg.setIcon(new ImageIcon("C:\\Users\\PRABAL MALLICK\\Desktop\\code prac\\FinalProject_WanderBd\\Image\\dream.PNG"));
		this.labelImg.setSize(800, 727);
		this.labelImg.setLocation(0, 0);
		this.panel.add(this.labelImg);
		
		this.labelTit = new JLabel("Wolcome to Wander BD");
		this.labelTit.setBounds(850, 50, 600, 50);
		this.labelTit.setFont(new Font("Andalus",Font.BOLD,40));
		this.labelTit.setForeground(new Color(0xF2F2F2));
		this.panel.add(this.labelTit);
		
		this.labelTit1 = new JLabel("Discover Bangladesh and Stay Connectet With Wander");
		this.labelTit1.setBounds(855, 90, 600, 50);
		this.labelTit1.setFont(new Font("Times New Roman",Font.PLAIN,20));
		this.labelTit1.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit1);
		
		this.labelTit2 = new JLabel("Bangladesh...");
		this.labelTit2.setBounds(855, 110, 600, 50);
		this.labelTit2.setFont(new Font("Times New Roman",Font.PLAIN,20));
		this.labelTit2.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit2);
		
		this.labelTit3 = new JLabel("Know More..Do More..Do Better");
		this.labelTit3.setBounds(895, 190, 600, 50);
		this.labelTit3.setFont(new Font("Times New Roman",Font.PLAIN,28));
		this.labelTit3.setForeground(new Color(70,90,60,250));
		this.panel.add(this.labelTit3);
		
		
		this.labelUser = new JLabel("Username");
		this.labelUser.setBounds(860, 340, 130, 25);
		this.labelUser.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelUser.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelUser);
		
		this.textUser = new JTextField();
		this.textUser.setBounds(980, 340, 260, 25);
		this.textUser.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textUser);
		
		
		this.labelPass = new JLabel("Password");
		this.labelPass.setBounds(860, 390, 130, 25);
		this.labelPass.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelPass.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPass);
		
		this.textPass = new JPasswordField();
		this.textPass.setBounds(980, 390, 260, 25);
		this.textPass.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,20));
		this.panel.add(this.textPass);
		
		this.labelTit3 = new JLabel("Need an Account? Click");
		this.labelTit3.setBounds(960, 650, 170, 25);
		this.labelTit3.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,12));
		this.labelTit3.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit3);
		
		this.buttonSingin = new JButton("Sign In");
		this.buttonSingin.setBounds(1015, 480, 120, 30);
		this.buttonSingin.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonSingin.setBackground(new Color(0x260000));
		this.buttonSingin.setForeground(new Color(0xF2E6E6));
		this.buttonSingin.addActionListener(this);
		this.panel.add(this.buttonSingin);
		
		this.buttonHere = new JButton("Here");
		this.buttonHere.setBounds(1100, 653, 62, 18);
		this.buttonHere.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,12));
		this.buttonHere.setBackground(new Color(0x333333));
		this.buttonHere.setForeground(new Color(0xFFFFFF));
		this.buttonHere.addActionListener(this);
		this.panel.add(this.buttonHere);
		
		this.add(this.panel);
		this.panel.add(this.panel1);
		
		this.setSize(1366, 727);
		this.setTitle("Sing In");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		validate();
	}
}
