import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;



public class listUpdate extends JFrame implements ActionListener
{
		private JPanel panel,panel1,panel2,panel3;
		private JLabel labelW,labelA,labelN,labelD,labelE,labelR,labelBd,labelTit1,labelTit2,labelTit3;
		private JTable table;
		private JScrollPane scrollPane;
		private DefaultTableModel tableModel;
		private JButton buttonExit, buttonUpdate;
		private JLabel labelDist,labelEstablis, labelDiv, labelArea, labelPopul, labelLitarecy, labelHtplace;
		private JTextField textDist,textEstablis,textDiv,textArea,textPopul,textLitarecy,textHtplace;
		private data1 dat1;
		private String disname,division,place,estb,area,popul,litarc;
		Connection con;
		PreparedStatement pst;
		ResultSet rs;
		
		
		public listUpdate(String disname,String estb,String division,String area,String popul,String litarc,String place*/)
		{
			this.dat1 = dat1;
			this.disname = disname;
			this.estb = estb;
			this.division = division;
			this.area = area;
			this.popul = popul;
			this.litarc = litarc;
			this.place = place;
			this.searchResult();
		}
		
		public void actionPerformed(ActionEvent event)
		{
			String buttPress = event.getActionCommand();
			if(buttPress.equals("Exit"))
			{
				System.exit(0);
			}
			else if(buttPress.equals("Update"))
			{
					data1 dat1 = new data1();
					dat1.setDistName(textDist.getSelectedText());
					dat1.setEstablish(textEstablis.getSelectedText());
					dat1.setDivision(textDiv.getSelectedText());
					dat1.setArea(textArea.getSelectedText());
					dat1.setPopulation(textPopul.getSelectedText());
					dat1.setLitarecy(textLitarecy.getSelectedText());
					dat1.setHistorical(textHtplace.getSelectedText());
					context con = new context();
					con.update(dat1);
					
					
					//Class.forName("com.mysql.jdbc.Driver");
		           // con=DriverManager.getConnection("jdbc:mysql://localhost:3306/finalproject", "root", "");
					
					//String sql = "update public set dname ='"+disname+"', establish = '"+estb+"', division = '"+division+"', area= '"+area+"', population= '"+popul+"', litarecy= '"+litarc+"',place = '"+place+"' where dname LIKE '" + this.disname + "' or establish LIKE '" + this.estb + "' or division LIKE '" + this.division + "' or area LIKE '" + this.area + "' or population LIKE '" + this.popul + "' or litarecy LIKE '" + this.litarc + "' or place LIKE '" + this.place + "'";
		            //pst = con.prepareStatement(sql);
		            //pst.executeUpdate();
		            
		            JOptionPane.showMessageDialog(rootPane, "Updated");
		            this.dispose();
			}
		}
		
		public void searchResult()
		{
			this.panel = new JPanel();
			this.panel.setBackground(Color.black);
			this.panel.setLayout(null);
			
			this.panel1 = new JPanel();
			this.panel1.setBackground(new Color(0x00000A));
			this.panel1.setSize(1366, 65);
			this.panel1.setLocation(0, 0);
			this.panel1.setLayout(null);
			
			this.panel2 = new JPanel();
			this.panel2.setBackground(new Color(0x080800));
			this.panel2.setSize(750, 560);
			this.panel2.setLocation(585, 65);
			this.panel2.setLayout(null);
			
			this.panel3 = new JPanel();
			this.panel3.setBackground(new Color(0x00000A));
			this.panel3.setSize(1366, 65);
			this.panel3.setLocation(0, 625);
			this.panel3.setLayout(null);
			
			this.labelW = new JLabel("W");
			this.labelW.setBounds(210,8, 600, 50);
			this.labelW.setFont(new Font("Snap ITC",Font.PLAIN,40));
			this.labelW.setForeground(Color.BLUE);
			this.panel.add(this.labelW);
			
			this.labelA = new JLabel("a");
			this.labelA.setBounds(250, 18, 100, 30);
			this.labelA.setFont(new Font("Jokerman",Font.PLAIN,35));
			this.labelA.setForeground(Color.RED);
			this.panel.add(this.labelA);
			
			this.labelN = new JLabel("n");
			this.labelN.setBounds(268, 18, 100, 30);
			this.labelN.setFont(new Font("Jokerman",Font.PLAIN,35));
			this.labelN.setForeground(Color.GREEN);
			this.panel.add(this.labelN);
			
			this.labelD = new JLabel("d");
			this.labelD.setBounds(287, 19, 100, 30);
			this.labelD.setFont(new Font("Jokerman",Font.PLAIN,35));
			this.labelD.setForeground(Color.DARK_GRAY);
			this.panel.add(this.labelD);
			
			this.labelE = new JLabel("e");
			this.labelE.setBounds(307, 18, 100, 30);
			this.labelE.setFont(new Font("Jokerman",Font.PLAIN,35));
			this.labelE.setForeground(Color.orange);
			this.panel.add(this.labelE);
			
			this.labelR = new JLabel("R");
			this.labelR.setBounds(327, 18, 100, 30);
			this.labelR.setFont(new Font("Snap ITC",Font.PLAIN,40));
			this.labelR.setForeground(Color.BLUE);
			this.panel.add(this.labelR);
			
			this.labelBd = new JLabel(" BD");
			this.labelBd.setBounds(357, 30, 100, 30);
			this.labelBd.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,30));
			this.labelBd.setForeground(new Color(0xCCCCA3));
			this.panel.add(this.labelBd);
			
			context con = new context();
			Vector<String> columns = new Vector<String>();
			columns.add("District Name");
			columns.add("Establishment");
			columns.add("Division");
			columns.add("Area");
			columns.add("Population");
			columns.add("Literacy Rate");
			columns.add("Historical Place");
			this.tableModel = new DefaultTableModel(con.getdataListAsString(), columns);
			
			this.table = new JTable(this.tableModel);
			this.table.getColumnModel().getColumn(0).setPreferredWidth(90);
			this.table.getColumnModel().getColumn(1).setPreferredWidth(50);
			this.table.getColumnModel().getColumn(2).setPreferredWidth(50);
			this.table.getColumnModel().getColumn(3).setPreferredWidth(50);
			this.table.getColumnModel().getColumn(4).setPreferredWidth(70);
			this.table.getColumnModel().getColumn(5).setPreferredWidth(40);
			this.table.getColumnModel().getColumn(6).setPreferredWidth(130);
			this.table.setRowSelectionAllowed(true);
			this.table.setUpdateSelectionOnSort(true);
			
			this.labelDist = new JLabel("District Name (String)");
			this.labelDist.setBounds(10, 180, 200, 25);
			this.labelDist.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.labelDist.setForeground(new Color(0xffffc1));
			this.panel.add(this.labelDist);
			
			this.textDist = new JTextField();
			this.textDist.setBounds(240, 180, 300, 25);
			this.textDist.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.panel.add(this.textDist);
			
			this.labelEstablis = new JLabel("Establishment (int)");
			this.labelEstablis.setBounds(10, 220, 200, 25);
			this.labelEstablis.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.labelEstablis.setForeground(new Color(0xffffc1));
			this.panel.add(this.labelEstablis);
			
			this.textEstablis = new JTextField();
			this.textEstablis.setBounds(240, 220, 200, 25);
			this.textEstablis.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.panel.add(this.textEstablis);
			
			this.labelDiv = new JLabel("Division (String)");
			this.labelDiv.setBounds(10, 260, 200, 25);
			this.labelDiv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.labelDiv.setForeground(new Color(0xffffc1));
			this.panel.add(this.labelDiv);
			
			this.textDiv = new JTextField();
			this.textDiv.setBounds(240, 260, 200, 25);
			this.textDiv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.panel.add(this.textDiv);
			
			this.labelArea = new JLabel("Area (float)");
			this.labelArea.setBounds(10, 300, 200, 25);
			this.labelArea.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.labelArea.setForeground(new Color(0xffffc1));
			this.panel.add(this.labelArea);
			
			this.textArea = new JTextField();
			this.textArea.setBounds(240, 300, 200, 25);
			this.textArea.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.panel.add(this.textArea);
			
			this.labelPopul = new JLabel("Population (float)");
			this.labelPopul.setBounds(10, 340, 200, 25);
			this.labelPopul.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.labelPopul.setForeground(new Color(0xffffc1));
			this.panel.add(this.labelPopul);
			
			this.textPopul = new JTextField();
			this.textPopul.setBounds(240, 340, 200, 25);
			this.textPopul.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.panel.add(this.textPopul);
			
			this.labelLitarecy = new JLabel("Literacy Rate (float)");
			this.labelLitarecy.setBounds(10, 380, 200, 25);
			this.labelLitarecy.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.labelLitarecy.setForeground(new Color(0xffffc1));
			this.panel.add(this.labelLitarecy);
			
			this.textLitarecy = new JTextField();
			this.textLitarecy.setBounds(240, 380, 200, 25);
			this.textLitarecy.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.panel.add(this.textLitarecy);
			
			this.labelHtplace = new JLabel("Historical Place (String)");
			this.labelHtplace.setBounds(10, 420, 200, 25);
			this.labelHtplace.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.labelHtplace.setForeground(new Color(0xffffc1));
			this.panel.add(this.labelHtplace);
			
			this.textHtplace = new JTextField();
			this.textHtplace.setBounds(240, 420, 300, 50);
			this.textHtplace.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
			this.panel.add(this.textHtplace);
	
			
			this.buttonUpdate = new JButton("Update");
			this.buttonUpdate.setBounds(700, 640, 120, 30);
			this.buttonUpdate.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
			this.buttonUpdate.setBackground(new Color(0x260000));
			this.buttonUpdate.setForeground(new Color(0xF2E6E6));
			this.buttonUpdate.addActionListener(this);
			this.panel.add(this.buttonUpdate);
			
			this.buttonExit = new JButton("Exit");
			this.buttonExit.setBounds(540, 640, 120, 30);
			this.buttonExit.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
			this.buttonExit.setBackground(new Color(0x260000));
			this.buttonExit.setForeground(new Color(0xF2E6E6));
			this.buttonExit.addActionListener(this);
			this.panel.add(this.buttonExit);
			
			this.scrollPane = new JScrollPane(this.table);
			this.scrollPane.setBounds(585, 150, 750, 400);
			this.panel.add(this.scrollPane);
			
			this.add(this.panel);
			this.panel.add(this.panel1);
			this.panel.add(this.panel2);
			this.panel.add(this.panel3);
			
			this.setSize(1366, 727);
			this.setTitle("Search Result");
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
}
