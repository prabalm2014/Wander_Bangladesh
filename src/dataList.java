import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;


public class dataList extends JFrame implements ActionListener{
	
	private JPanel panel,panel1,panel2,panel3;
	private JLabel labelW,labelA,labelN,labelD,labelE,labelR,labelBd,labelTit2,labelTit3;
	private JTable table;
	private JScrollPane scrollPane;
	private DefaultTableModel tableModel;
	private JButton buttonBack;
	
	
	public dataList()
	{
		this.searchResult();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Back"))
		{
			afterLogin al = new afterLogin();
			al.setVisible(true);
			this.setVisible(false);
		}
	}
	
	public void searchResult()
	{
		this.panel = new JPanel();
		this.panel.setBackground(Color.black);
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(0x00000A));
		this.panel1.setSize(1366, 65);
		this.panel1.setLocation(0, 0);
		this.panel1.setLayout(null);
		
		this.panel2 = new JPanel();
		this.panel2.setBackground(new Color(0x080800));
		this.panel2.setSize(1100, 560);
		this.panel2.setLocation(130, 65);
		this.panel2.setLayout(null);
		
		this.panel3 = new JPanel();
		this.panel3.setBackground(new Color(0x00000A));
		this.panel3.setSize(1366, 65);
		this.panel3.setLocation(0, 625);
		this.panel3.setLayout(null);
		
		this.labelW = new JLabel("W");
		this.labelW.setBounds(210,8, 600, 50);
		this.labelW.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelW.setForeground(Color.BLUE);
		this.panel.add(this.labelW);
		
		this.labelA = new JLabel("a");
		this.labelA.setBounds(250, 18, 100, 30);
		this.labelA.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelA.setForeground(Color.RED);
		this.panel.add(this.labelA);
		
		this.labelN = new JLabel("n");
		this.labelN.setBounds(268, 18, 100, 30);
		this.labelN.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelN.setForeground(Color.GREEN);
		this.panel.add(this.labelN);
		
		this.labelD = new JLabel("d");
		this.labelD.setBounds(287, 19, 100, 30);
		this.labelD.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelD.setForeground(Color.DARK_GRAY);
		this.panel.add(this.labelD);
		
		this.labelE = new JLabel("e");
		this.labelE.setBounds(307, 18, 100, 30);
		this.labelE.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelE.setForeground(Color.orange);
		this.panel.add(this.labelE);
		
		this.labelR = new JLabel("R");
		this.labelR.setBounds(327, 18, 100, 30);
		this.labelR.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelR.setForeground(Color.BLUE);
		this.panel.add(this.labelR);
		
		this.labelBd = new JLabel(" BD");
		this.labelBd.setBounds(357, 30, 100, 30);
		this.labelBd.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,30));
		this.labelBd.setForeground(new Color(0xCCCCA3));
		this.panel.add(this.labelBd);
		
		this.labelTit2 = new JLabel("Search Result");
		this.labelTit2.setBounds(590, 130, 800, 30);
		this.labelTit2.setFont(new Font("Andalus",Font.BOLD,30));
		this.labelTit2.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit2);
		
		context con = new context();
		Vector<String> columns = new Vector<String>();
		columns.add("District Name");
		columns.add("Establishment");
		columns.add("Division");
		columns.add("Area");
		columns.add("Population");
		columns.add("Literacy Rate");
		columns.add("Historical Place");
		this.tableModel = new DefaultTableModel(con.getdataListAsString(), columns);
		
		this.table = new JTable(this.tableModel);
		this.table.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.table.getColumnModel().getColumn(1).setPreferredWidth(50);
		this.table.getColumnModel().getColumn(2).setPreferredWidth(50);
		this.table.getColumnModel().getColumn(3).setPreferredWidth(50);
		this.table.getColumnModel().getColumn(4).setPreferredWidth(70);
		this.table.getColumnModel().getColumn(5).setPreferredWidth(40);
		this.table.getColumnModel().getColumn(6).setPreferredWidth(130);
		this.table.setRowSelectionAllowed(true);
		this.table.setUpdateSelectionOnSort(isForegroundSet());
		
		this.buttonBack = new JButton("Back");
		this.buttonBack.setBounds(640, 640, 120, 30);
		this.buttonBack.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonBack.setBackground(new Color(0x260000));
		this.buttonBack.setForeground(new Color(0xF2E6E6));
		this.buttonBack.addActionListener(this);
		this.panel.add(this.buttonBack);
		
		this.scrollPane = new JScrollPane(this.table);
		this.scrollPane.setBounds(130, 250, 1100, 200);
		this.panel.add(this.scrollPane);
		
		this.add(this.panel);
		this.panel.add(this.panel1);
		this.panel.add(this.panel2);
		this.panel.add(this.panel3);
		
		this.setSize(1366, 727);
		this.setTitle("Private Search Result");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
}
