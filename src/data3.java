import java.util.Vector;


public class data3 {
	
	public String distName,Division,Historical,Area,Population,Litarecy,id,Establish,search;

	public data3(String distName, String Establish, String Division, String Area,String Population, String Litarecy, String Historical) 
	{
		this.distName = distName;
		this.Establish = Establish;
		this.Division = Division;
		this.Area = Area;
		this.Population = Population;
		this.Litarecy = Litarecy;
		this.Historical = Historical;
	}

	public String getDistName() {
		return distName;
	}

	public String getDivision() {
		return Division;
	}

	public String getHistorical() {
		return Historical;
	}

	public String getArea() {
		return Area;
	}

	public String getPopulation() {
		return Population;
	}

	public String getLitarecy() {
		return Litarecy;
	}

	public String getId() {
		return id;
	}

	public String getEstablish() {
		return Establish;
	}

	public String getSearch() {
		return search;
	}

	public Vector<String> getVector() {
		Vector<String> vector = new Vector<String>();
        vector.add(this.distName);
        vector.add(this.Establish);
        vector.add(this.Division);
        vector.add(this.Area);
        vector.add(this.Population);
        vector.add(this.Litarecy);
        vector.add(this.Historical);
        return vector;
	}
}
