import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class serchEngin extends JFrame implements ActionListener
{
	private JPanel panel,panel1;
	private JLabel labelW,labelA,labelN,labelD,labelE,labelR,labelBd;
	private JTextField textSearch;
	private JButton buttonSingin,buttonSingup,buttonSearch,buttonAbout,buttonDevlp;
	private JTable table;
	private JScrollPane scrollPane;
	private DefaultTableModel tableModel;
	public serchEngin()
	{
		this.setUp();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Sign In"))
		{
			singIn si = new singIn();
			si.setVisible(true);
			this.setVisible(false);
		}
		else if(buttPress.equals("Sign Up"))
		{
			singUp su = new singUp();
			su.setVisible(true);
			this.setVisible(false);
		}
		else if(buttPress.equals("Search"))
		{	
			loginContext lCon = new loginContext();
			data1 dat1 = new data1();
			dat1.setSearch(textSearch.getText());
			String textSearch = this.textSearch.getText().trim();
			if(textSearch.equals(""))
			{
				JOptionPane.showMessageDialog(null, "Enter The District name");
			}
			else if(lCon.getCheckSerch(textSearch))
			{
				JOptionPane.showMessageDialog(null, "Ooopps ! Result Not Found For Construction Issues or Check Spelling.");
			}
			else if(textSearch.equals(textSearch))
			{
				context con = new context();
				Vector<String> columns = new Vector<String>();
				columns.add("District Name");
				columns.add("Establishment");
				columns.add("Division");
				columns.add("Area(km2)");
				columns.add("Population");
				columns.add("Literacy Rate(%)");
				columns.add("Historical Place");
				this.tableModel = new DefaultTableModel(con.getpublicSearchAsString(dat1),columns);
				
				this.table = new JTable(this.tableModel);
				this.table.getColumnModel().getColumn(0).setPreferredWidth(90);
				this.table.getColumnModel().getColumn(1).setPreferredWidth(50);
				this.table.getColumnModel().getColumn(2).setPreferredWidth(50);
				this.table.getColumnModel().getColumn(3).setPreferredWidth(30);
				this.table.getColumnModel().getColumn(4).setPreferredWidth(80);
				this.table.getColumnModel().getColumn(5).setPreferredWidth(30);
				this.table.getColumnModel().getColumn(6).setPreferredWidth(250);
				this.table.setRowSelectionAllowed(true);
				this.table.setEnabled(false);
				
				this.scrollPane = new JScrollPane(this.table);
				this.scrollPane.setBounds(25, 400, 1300, 39);
				this.panel.add(this.scrollPane);
			}
		}
		else if(buttPress.equals("Developer"))
		{
			developer dv = new developer();
			dv.setVisible(true);
			this.setVisible(false);
		}
		else if(buttPress.equals("About"))
		{
			about ab = new about();
			ab.setVisible(true);
			this.setVisible(false);
		}
	}
	
	public void setUp()
	{
		this.panel = new JPanel();
		this.panel.setBackground(Color.WHITE);
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(20, 30, 14, 23));
		this.panel1.setSize(1360, 80);
		this.panel1.setLocation(0, 618);
		this.panel1.setLayout(null);
		
		this.labelW = new JLabel("W");
		this.labelW.setBounds(490, 120, 120, 100);
		this.labelW.setFont(new Font("Snap ITC",Font.PLAIN,100));
		this.labelW.setForeground(Color.blue);
		this.panel.add(this.labelW);
		
		this.labelA = new JLabel("a");
		this.labelA.setBounds(585, 124, 100, 100);
		this.labelA.setFont(new Font("Jokerman",Font.PLAIN,85));
		this.labelA.setForeground(Color.RED);
		this.panel.add(this.labelA);
		
		
		this.labelN = new JLabel("n");
		this.labelN.setBounds(628, 125, 100, 100);
		this.labelN.setFont(new Font("Jokerman",Font.PLAIN,85));
		this.labelN.setForeground(Color.GREEN);
		this.panel.add(this.labelN);
		
		this.labelD = new JLabel("d");
		this.labelD.setBounds(672, 125, 100, 100);
		this.labelD.setFont(new Font("Jokerman",Font.PLAIN,90));
		this.labelD.setForeground(Color.DARK_GRAY);
		this.panel.add(this.labelD);
		
		this.labelE = new JLabel("e");
		this.labelE.setBounds(720, 120, 100, 100);
		this.labelE.setFont(new Font("Jokerman",Font.PLAIN,90));
		this.labelE.setForeground(Color.orange);
		this.panel.add(this.labelE);
		
		this.labelR = new JLabel("R");
		this.labelR.setBounds(767, 126, 200, 100);
		this.labelR.setFont(new Font("Snap ITC",Font.PLAIN,80));
		this.labelR.setForeground(Color.BLUE);
		this.panel.add(this.labelR);
		
		this.labelBd = new JLabel("Bangladesh");
		this.labelBd.setBounds(775, 160, 200, 110);
		this.labelBd.setFont(new Font("Eras Demi ITC",Font.BOLD,23));
		this.labelBd.setBackground(new Color(0x330000));
		this.panel.add(this.labelBd);
		
		this.textSearch = new JTextField();
		this.textSearch.setBounds(385, 240, 590, 30);
		this.textSearch.setCaretColor(Color.BLUE);
		this.textSearch.setFont(new Font("Calibri Body",Font.PLAIN,18));
		this.textSearch.setToolTipText("Write Dstrict Name Here");
		this.panel.add(this.textSearch);
		
		this.buttonAbout = new JButton("About");
		this.buttonAbout.setBounds(20, 640, 100, 28);
		this.buttonAbout.setBackground(new Color(0xBDEDFF));
		this.buttonAbout.addActionListener(this);
		this.panel.add(this.buttonAbout);
		
		this.buttonDevlp = new JButton("Developer");
		this.buttonDevlp.setBounds(135, 640, 100, 28);
		this.buttonDevlp.setBackground(new Color(0xBDEDFF));
		this.buttonDevlp.addActionListener(this);
		this.panel.add(this.buttonDevlp);
		
		this.buttonSearch = new JButton("Search");
		this.buttonSearch.setBounds(620, 285, 120, 32);
		this.buttonSearch.setFont(new Font("normal",Font.PLAIN,18));
		this.buttonSearch.setBackground(new Color(0xE0FFFF));
		this.buttonSearch.addActionListener(this);
		this.panel.add(this.buttonSearch);
		
		
		this.buttonSingin = new JButton("Sign In");
		this.buttonSingin.setBounds(1220, 20, 90, 28);
		this.buttonSingin.setBackground(new Color(0xEBF4FA));
		this.buttonSingin.addActionListener(this);
		this.panel.add(this.buttonSingin);
		
		this.buttonSingup = new JButton("Sign Up");
		this.buttonSingup.setBounds(1120, 20, 90, 28);
		this.buttonSingup.setBackground(new Color(0xEBF4FA));
		this.buttonSingup.addActionListener(this);
		this.panel.add(this.buttonSingup);
		
		this.add(this.panel);
		this.panel.add(this.panel1);
	
		
		this.setSize(1366, 727);
		this.setTitle("Wander BD");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
