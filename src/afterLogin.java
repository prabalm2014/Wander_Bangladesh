import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.tools.OptionChecker;

public class afterLogin extends JFrame implements ActionListener{
	private JPanel panel,panel1,panel2,panel3;
	private JLabel labelW,labelA,labelN,labelD,labelE,labelR,labelBd,labelTit1,labelTit2,labelTit3,labelTit4,labelTit5;
	private JButton buttonExit, buttonUpdate,buttonInsert,buttonDtlist,buttonLogout;

	public afterLogin()
	{
		this.searchResult();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Exit"))
		{
			System.exit(0);
		}
		else if(buttPress.equals("Update"))
		{
			this.setVisible(false);
		}
		else if(buttPress.equals("Insert"))
		{
			input ip = new input();
			ip.setVisible(true);
			this.setVisible(false);
		}
		else if(buttPress.equals("Preview"))
		{
			dataList dl = new dataList();
			dl.setVisible(true);
			this.setVisible(false);
		}
		else if(buttPress.equals("Log out"))
		{
			JOptionPane.showMessageDialog(null, "Successfully Log Out !");
			singIn si = new singIn();
			si.setVisible(true);
			this.setVisible(false);
			
		    int response = JOptionPane.showConfirmDialog(null, "Back to Home", "Confirm",
		        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		    if (response == JOptionPane.NO_OPTION) 
		    {
		    	si.setVisible(true);
		    } 
		    else if (response == JOptionPane.YES_OPTION) 
		    {
		    	serchEngin se = new serchEngin();
				se.setVisible(true);
				this.setVisible(false);
		    } 
		    else if (response == JOptionPane.CANCEL_OPTION) 
		    {
		       si.dispose();
		    }
		}
	}
	
	public void searchResult()
	{
		this.panel = new JPanel();
		this.panel.setBackground(Color.black);
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(0x00000A));
		this.panel1.setSize(1366, 65);
		this.panel1.setLocation(0, 0);
		this.panel1.setLayout(null);
		
		this.panel2 = new JPanel();
		this.panel2.setBackground(new Color(0x080800));
		this.panel2.setSize(400, 560);
		this.panel2.setLocation(840, 65);
		this.panel2.setLayout(null);
		
		this.panel3 = new JPanel();
		this.panel3.setBackground(new Color(0x00000A));
		this.panel3.setSize(1366, 65);
		this.panel3.setLocation(0, 625);
		this.panel3.setLayout(null);
		
		this.labelW = new JLabel("W");
		this.labelW.setBounds(210,8, 600, 50);
		this.labelW.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelW.setForeground(Color.BLUE);
		this.panel.add(this.labelW);
		
		this.labelA = new JLabel("a");
		this.labelA.setBounds(250, 18, 100, 30);
		this.labelA.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelA.setForeground(Color.RED);
		this.panel.add(this.labelA);
		
		this.labelN = new JLabel("n");
		this.labelN.setBounds(268, 18, 100, 30);
		this.labelN.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelN.setForeground(Color.GREEN);
		this.panel.add(this.labelN);
		
		this.labelD = new JLabel("d");
		this.labelD.setBounds(287, 19, 100, 30);
		this.labelD.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelD.setForeground(Color.DARK_GRAY);
		this.panel.add(this.labelD);
		
		this.labelE = new JLabel("e");
		this.labelE.setBounds(307, 18, 100, 30);
		this.labelE.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelE.setForeground(Color.orange);
		this.panel.add(this.labelE);
		
		this.labelR = new JLabel("R");
		this.labelR.setBounds(327, 18, 100, 30);
		this.labelR.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelR.setForeground(Color.BLUE);
		this.panel.add(this.labelR);
		
		this.labelBd = new JLabel(" BD");
		this.labelBd.setBounds(357, 30, 100, 30);
		this.labelBd.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,30));
		this.labelBd.setForeground(new Color(0xCCCCA3));
		this.panel.add(this.labelBd);
		
		this.labelTit1 = new JLabel("Welcome To Wander Bangladesh");
		this.labelTit1.setBounds(240, 160, 360, 35);
		this.labelTit1.setFont(new Font("Freestyle Script",Font.BOLD,34));
		this.labelTit1.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit1);
		
		this.labelTit2 = new JLabel("Thank You For Being With Us .You Can Insert New And Updated Information By Clicking Insert Button.");
		this.labelTit2.setBounds(40, 230, 800, 30);
		this.labelTit2.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelTit2.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit2);
		
		this.labelTit3 = new JLabel("You Can Also Recheck Your Inserted Data By Clicking Preview. After Inserting Data You Will Notify,That");
		this.labelTit3.setBounds(40, 250, 800, 30);
		this.labelTit3.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelTit3.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit3);
		
		this.labelTit4 = new JLabel("Your Data Is Acceptable or Not Within 24 Hours by Admin. After 72 Hours Your Data Will Remove From");
		this.labelTit4.setBounds(40, 270, 800, 30);
		this.labelTit4.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelTit4.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit4);
		
		this.labelTit5 = new JLabel("Public Database and Will Restore In The Main Database If Acceptable.");
		this.labelTit5.setBounds(40, 290, 800, 30);
		this.labelTit5.setFont(new Font("Andalus",Font.PLAIN,18));
		this.labelTit5.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelTit5);
		
		this.buttonInsert = new JButton("Insert");
		this.buttonInsert.setBounds(945, 240, 180, 30);
		this.buttonInsert.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonInsert.setBackground(new Color(0x00000A));
		this.buttonInsert.setForeground(new Color(0xF2E6E6));
		this.buttonInsert.addActionListener(this);
		this.panel.add(this.buttonInsert);
		
		this.buttonDtlist = new JButton("Preview");
		this.buttonDtlist.setBounds(945, 290, 180, 30);
		this.buttonDtlist.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonDtlist.setBackground(new Color(0x00000A));
		this.buttonDtlist.setForeground(new Color(0xF2E6E6));
		this.buttonDtlist.addActionListener(this);
		this.panel.add(this.buttonDtlist);
		
		this.buttonLogout = new JButton("Log out");
		this.buttonLogout.setBounds(945, 340, 180, 30);
		this.buttonLogout.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonLogout.setBackground(new Color(0x00000A));
		this.buttonLogout.setForeground(new Color(0xF2E6E6));
		this.buttonLogout.addActionListener(this);
		this.panel.add(this.buttonLogout);
		
		this.buttonExit = new JButton("Exit");
		this.buttonExit.setBounds(945, 450, 180, 30);
		this.buttonExit.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonExit.setBackground(new Color(0x00000A));
		this.buttonExit.setForeground(new Color(0xF2E6E6));
		this.buttonExit.addActionListener(this);
		this.panel.add(this.buttonExit);
		
		this.add(this.panel);
		this.panel.add(this.panel1);
		this.panel.add(this.panel2);
		this.panel.add(this.panel3);
		
		this.setSize(1366, 727);
		this.setTitle("Profile");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
