import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class forUpdate extends JFrame implements ActionListener{
	private JPanel panel,panel1,panel2;
	private JLabel labelW,labelA,labelN,labelD,labelE,labelR,labelBd,labelTit1, labelTit2,labelTit3;
	private JLabel labelDist,labelEstablis, labelDiv, labelArea, labelPopul, labelLitarecy, labelHtplace;
	private JTextField textDist,textEstablis,textDiv,textArea,textPopul,textLitarecy,textHtplace;
	private JButton buttonSubmit,buttonExit;
	private String disname,division,place,estb,area,popul,litarc;
	private dataList dlist;
	Connection con;
	PreparedStatement pst;
	ResultSet rs;

	public forUpdate(dataList dlist,String disname,String estb,String division,String area,String popul,String litarc,String place)
	{
		this.dlist = dlist;
		this.disname = disname;
		this.estb = estb;
		this.division = division;
		this.area = area;
		this.popul = popul;
		this.litarc = litarc;
		this.place = place;
		this.dataEntry();
	}
	
	public void actionPerformed(ActionEvent event)
	{
		String buttPress = event.getActionCommand();
		if(buttPress.equals("Update"))
		{
			
		}
		else if(buttPress.equals("Exit"))
		{
			System.exit(0);
		}
	}
	
	public void dataEntry()
	{
		this.panel = new JPanel();
		this.panel.setBackground(Color.black);
		this.panel.setLayout(null);
		
		this.panel1 = new JPanel();
		this.panel1.setBackground(new Color(0x00000A));
		this.panel1.setSize(1366, 75);
		this.panel1.setLocation(0, 0);
		this.panel1.setLayout(null);
		
		this.panel2 = new JPanel();
		this.panel2.setBackground(new Color(0x080800));
		this.panel2.setSize(800, 800);
		this.panel2.setLocation(600, 75);
		this.panel2.setLayout(null);
		
		this.labelW = new JLabel("W");
		this.labelW.setBounds(210, 15, 600, 50);
		this.labelW.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelW.setForeground(Color.BLUE);
		this.panel.add(this.labelW);
		
		this.labelA = new JLabel("a");
		this.labelA.setBounds(250, 25, 100, 30);
		this.labelA.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelA.setForeground(Color.RED);
		this.panel.add(this.labelA);
		
		this.labelN = new JLabel("n");
		this.labelN.setBounds(268, 25, 100, 30);
		this.labelN.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelN.setForeground(Color.GREEN);
		this.panel.add(this.labelN);
		
		this.labelD = new JLabel("d");
		this.labelD.setBounds(287, 27, 100, 30);
		this.labelD.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelD.setForeground(Color.DARK_GRAY);
		this.panel.add(this.labelD);
		
		this.labelE = new JLabel("e");
		this.labelE.setBounds(307, 25, 100, 30);
		this.labelE.setFont(new Font("Jokerman",Font.PLAIN,35));
		this.labelE.setForeground(Color.orange);
		this.panel.add(this.labelE);
		
		this.labelR = new JLabel("R");
		this.labelR.setBounds(327, 25, 100, 30);
		this.labelR.setFont(new Font("Snap ITC",Font.PLAIN,40));
		this.labelR.setForeground(Color.BLUE);
		this.panel.add(this.labelR);
		
		this.labelBd = new JLabel(" BD");
		this.labelBd.setBounds(357, 40, 100, 30);
		this.labelBd.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,30));
		this.labelBd.setForeground(new Color(0xCCCCA3));
		this.panel.add(this.labelBd);
		
		this.labelDist = new JLabel("District Name (String)");
		this.labelDist.setBounds(700, 185, 200, 25);
		this.labelDist.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelDist.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelDist);
		
		this.textDist = new JTextField();
		this.textDist.setBounds(920, 185, 300, 25);
		this.textDist.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textDist);
		
		this.labelEstablis = new JLabel("Establishment (int)");
		this.labelEstablis.setBounds(700, 225, 200, 25);
		this.labelEstablis.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelEstablis.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelEstablis);
		
		this.textEstablis = new JTextField();
		this.textEstablis.setBounds(920, 225, 200, 25);
		this.textEstablis.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textEstablis);
		
		this.labelDiv = new JLabel("Division (String)");
		this.labelDiv.setBounds(700, 265, 200, 25);
		this.labelDiv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelDiv.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelDiv);
		
		this.textDiv = new JTextField();
		this.textDiv.setBounds(920, 265, 200, 25);
		this.textDiv.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textDiv);
		
		this.labelArea = new JLabel("Area (float)");
		this.labelArea.setBounds(700, 305, 200, 25);
		this.labelArea.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelArea.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelArea);
		
		this.textArea = new JTextField();
		this.textArea.setBounds(920, 305, 200, 25);
		this.textArea.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textArea);
		
		this.labelPopul = new JLabel("Population (float)");
		this.labelPopul.setBounds(700, 345, 200, 25);
		this.labelPopul.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelPopul.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelPopul);
		
		this.textPopul = new JTextField();
		this.textPopul.setBounds(920, 345, 200, 25);
		this.textPopul.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textPopul);
		
		this.labelLitarecy = new JLabel("Literacy Rate (float)");
		this.labelLitarecy.setBounds(700, 385, 200, 25);
		this.labelLitarecy.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelLitarecy.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelLitarecy);
		
		this.textLitarecy = new JTextField();
		this.textLitarecy.setBounds(920, 385, 200, 25);
		this.textLitarecy.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textLitarecy);
		
		this.labelHtplace = new JLabel("Historical Place (String)");
		this.labelHtplace.setBounds(700, 430, 200, 25);
		this.labelHtplace.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.labelHtplace.setForeground(new Color(0xffffc1));
		this.panel.add(this.labelHtplace);
		
		this.textHtplace = new JTextField();
		this.textHtplace.setBounds(920, 430, 300, 50);
		this.textHtplace.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,16));
		this.panel.add(this.textHtplace);
		
		this.buttonSubmit = new JButton("Update");
		this.buttonSubmit.setBounds(1020, 580, 120, 30);
		this.buttonSubmit.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonSubmit.setBackground(new Color(0x260000));
		this.buttonSubmit.setForeground(new Color(0xF2E6E6));
		this.buttonSubmit.addActionListener(this);
		this.panel.add(this.buttonSubmit);
		
		this.buttonExit = new JButton("Exit");
		this.buttonExit.setBounds(840, 580, 120, 30);
		this.buttonExit.setFont(new Font("Arial Rounded MT Bold",Font.PLAIN,17));
		this.buttonExit.setBackground(new Color(0x260000));
		this.buttonExit.setForeground(new Color(0xF2E6E6));
		this.buttonExit.addActionListener(this);
		this.panel.add(this.buttonExit);

		this.add(this.panel);
		this.panel.add(this.panel1);
		this.panel.add(this.panel2);
		
		this.setSize(1366, 727);
		this.setTitle("For Update");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
