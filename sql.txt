CREATE TABLE `finalproject`.`singup` (
`SL` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 40 ) NOT NULL ,
`gender` VARCHAR( 10 ) NOT NULL ,
`email` VARCHAR( 40 ) NOT NULL ,
`pass` VARCHAR( 30 ) NOT NULL ,
UNIQUE (
`email`
)
) ENGINE = MYISAM ;

CREATE TABLE `finalproject`.`admin` (
`sl` INT NOT NULL AUTO_INCREMENT ,
`dname` VARCHAR( 30 ) NOT NULL ,
`establish` INT( 10 ) NOT NULL ,
`division` VARCHAR( 30 ) NOT NULL ,
`area` FLOAT( 20 ) NOT NULL ,
`population` FLOAT( 20 ) NOT NULL ,
`litarecy` FLOAT( 20 ) NOT NULL ,
`place` VARCHAR( 50 ) NOT NULL ,
PRIMARY KEY ( `sl` )
) ENGINE = MYISAM ;

CREATE TABLE `finalproject`.`public` (
`id` INT NOT NULL AUTO_INCREMENT ,
`dname` VARCHAR( 30 ) NOT NULL ,
`establish` INT( 10 ) NOT NULL ,
`division` VARCHAR( 30 ) NOT NULL ,
`area` FLOAT( 20 ) NOT NULL ,
`population` FLOAT( 20 ) NOT NULL ,
`litarecy` FLOAT( 20 ) NOT NULL ,
`place` VARCHAR( 50 ) NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM ;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `finalproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `sl` int(11) NOT NULL AUTO_INCREMENT,
  `dname` varchar(30) NOT NULL,
  `establish` int(10) NOT NULL,
  `division` varchar(30) NOT NULL,
  `area` float NOT NULL,
  `population` int(15) NOT NULL,
  `litarecy` float NOT NULL,
  `place` varchar(100) NOT NULL,
  PRIMARY KEY (`sl`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`sl`, `dname`, `establish`, `division`, `area`, `population`, `litarecy`, `place`) VALUES
(1, 'Dhaka', 1772, 'Dhaka', 1464, 11875000, 64.79, 'Lalbagh Fort,Ahsan Manzil,Moriam Saleh Mosque'),
(2, 'Gazipur', 1984, 'Dhaka', 1800, 3333000, 56.4, 'Ansar VDP,BRRI,BARI,National Park,Vawal Rajbari'),
(3, 'Narayanganj ', 1984, 'Dhaka', 700, 2897000, 51.75, 'Sultan Giasuddin Tomb,Shonarga,Goaldi Mosque '),
(4, 'Narsingdi', 1984, 'Dhaka', 1141, 2202000, 42.91, 'Raghunath Mandir,Three Domed Mosque '),
(5, 'Munshiganj', 1984, 'Dhaka', 955, 1420000, 51.62, 'Sunakanda Fort,Pagla Bridge,Kadam Rasul Dargah'),
(6, 'Manikganj', 1984, 'Dhaka', 1379, 1379000, 41.02, 'Matta Math,Teota Zamindar Bari,Baliati Zamindar Palace'),
(7, 'Tangail', 1969, 'Dhaka', 3414, 3571000, 40.64, 'Atia Mosque,Sagardighi,Ichamati Dighi,Nagarpur Dighi'),
(8, 'Mymensingh', 1787, 'Dhaka', 4363, 5042000, 39.11, 'Shashi Lodge,Musium,Durgabari,Mymensingh Town Hall'),
(9, 'Jamalpur', 1978, 'Dhaka', 2032, 2265000, 31.8, 'Shah Jamal Tomb,Shah Kamal Tomb,Dayamoye Mondir'),
(10, 'Sherpur', 1984, 'Dhaka', 1364, 1334000, 31.89, 'Sher Ali Gazi Tomb,Maisaheba Mosque'),
(11, 'Netrakona', 1984, 'Dhaka', 2810, 2207000, 34.94, 'Garo Pahar,Khejor Dighi,Krishnapur Boudhho Math'),
(12, 'Kishoreganj', 1984, 'Dhaka', 2689, 2853000, 38.27, 'jangalbari Fort,Egarasindhur Fort,Badshahi Mosque '),
(13, 'Shariatpur', 1984, 'Dhaka', 1182, 1146000, 38.95, 'South Baluchara Mosque,Bilaskhan Mosque,Mohishar Dighi '),
(14, 'Madaripur', 1984, 'Dhaka', 1145, 1149000, 42.14, 'Aoliapur Neelkuti,Algi Kazibadi Mosque,Ram Mandir '),
(15, 'Gopalganj', 1984, 'Dhaka', 1490, 1149000, 51.37, 'Gaohor Danga Madrasa,Madhumati River,Bornir Baor'),
(16, 'Faridpur', 1815, 'Dhaka', 2073, 1867000, 40.85, 'Garoda Mosque,Fatehabad Taksal,Mathurapur Wall'),
(17, 'Rajbari', 1984, 'Dhaka', 1119, 1040000, 39.81, 'Laxmikul Rajbari,Baniboh Jamidar Bari'),
(18, 'Chittagong', 1666, 'Chittagong ', 5283, 7509000, 55.54, 'Kala Mosque,Potenga Shomudrro Shaikwat'),
(19, 'Coxs Bazar', 1984, 'Chittagong', 2492, 2275000, 30.17, 'Largest Sea Beach in Asia,Enani Sea Beach,Sent Martin'),
(20, 'Bandarban', 1984, 'Chittagong', 4479, 383000, 31.66, 'Largest Hill Tajingdong,Bogaleg,Prantik Leg'),
(21, 'Rangamati', 1860, 'Chittagong', 6116, 596000, 43.59, 'Dighi and Mosque of Raja Jan Bashk Khan,Hanging Bridge'),
(22, 'Khagrachhari', 1984, 'Chittagong', 2700, 608000, 81.8, 'Alutila,Yonged Buddha Bihar,Dighinala Touduchhori Waterfall'),
(23, 'Feni', 1984, 'Chittagong', 928, 1420000, 54.26, 'Chandgazi Mosque,Bijoy Singh Dighi at Mohipal'),
(24, 'Noakhali', 1821, 'Chittagong', 3601, 3072000, 51.67, 'Noakhali Public Library,Nijhum Dweep,Gandhi Ashram'),
(25, 'Lakshmipur', 1984, 'Chittagong', 1456, 1711000, 42.93, 'Dalal Bazar Math,Khoa-sagor Dighi,Oidara Dighi'),
(26, 'Chandpur', 1984, 'Chittagong', 1704, 2393000, 50.29, 'Chandpur River Port,River Research Institute,Tultuli Ashram'),
(27, 'Comilla', 1790, 'Chittagong', 3085, 5304000, 45.98, 'Kutila Mura,Charandra Mura,Ranir Banglar Pahar,Ananda Bazar'),
(28, 'Brahmanbaria', 1984, 'Chittagong', 1927, 2808000, 39.45, 'Kal Bhairab Temple,Jame Mosque of Sarail '),
(29, 'Rajshahi', 1772, 'Rajshahi', 2407, 2573000, 47.54, 'Rajbari,Govinda Mandir,Shiva Mandir,Gopal Mandir '),
(30, 'Chapainawabganj', 1984, 'Rajshahi', 1703, 1635000, 35.92, 'Nachol Rajbari,MRI,Shona Moshjid'),
(31, 'Naogaon', 1984, 'Rajshahi', 3436, 2576000, 44.39, 'Paharpur Bouddho Bihar,Kushumba Moshjid'),
(32, 'Natore', 1984, 'Rajshahi', 1896, 1696000, 41.55, 'Natore Rajbari,Dighapatia Rajbari'),
(33, 'Pabna', 1832, 'Rajshahi', 2372, 2497000, 42.44, 'Snana Mandir,Chatmohar Shahi Mosque,Jagannath Mandir'),
(34, 'Sirajganj', 1984, 'Rajshahi', 2498, 3072000, 40.59, 'Shiva Mandir,Rabindra Kuthi Bari,Protap Dighi'),
(35, 'Bogra', 1821, 'Rajshahi', 2920, 3371000, 42.89, 'Vasu Vihara,Khandakartala Mosque,Bara Masque'),
(36, 'Joypurhat', 1984, 'Rajshahi', 965, 909000, 49.62, 'Nimai Pir Dargah,Remains This Palace of Raja Jaygopal'),
(37, 'Rangpur', 1769, 'Rangpur', 2368, 2866000, 41.91, 'Tajhat Rajbari,Keramatia Mosque,Dimlaraj Kali Mandir'),
(38, 'Kurigram', 1984, 'Rangpur', 2296, 2050000, 33.45, 'Joymoni Zamindar Bari,Kamakkha Devi,Mangal Chandi'),
(39, 'Gaibandha', 1984, 'Rangpur', 2179, 2349000, 35.73, 'Vardhan Kuthi,Gobindaganj,Masta Mosque'),
(40, 'Lalmonirhat', 1984, 'Rangpur', 1241, 1249000, 42.33, 'Sindhumati Dighi,Hussain Sarabor,Kakina Rajbari'),
(41, 'Nilphamari', 1984, 'Rangpur', 1581, 1820000, 38.84, 'Birat Dighi,Neel Kuthi,Dimla Rajbari,Saidpur Church'),
(42, 'Dinajpur', 1786, 'Rangpur', 3438, 2970000, 45.67, 'Kantanagar Temple,Dinajpur Museum,Shingha Darwaza Palace'),
(43, 'Thakurgaon', 1984, 'Rangpur', 1810, 1380000, 41.82, 'Rajbari of Raja Ganesh,Jagadal Rajbari,Bangla Garh'),
(44, 'Panchagarh', 1984, 'Rangpur', 1405, 981000, 43.89, 'Tajhat Rajbari,Keramatia Mosque,Dimlaraj Kali Mandir'),
(45, 'Khulna', 1882, 'Khulna', 4394, 2294000, 57.8, 'Sundarbans largest Mangrove Forest,Chitra Deer,Crocodiles'),
(46, 'Satkhira', 1984, 'Khulna', 3858, 1973000, 45.51, 'Dargah of Mai Champa,Baro Duary,Gopalpur Mandir'),
(47, 'Bagerhat', 1984, 'Khulna', 3959, 1461000, 58.73, 'Shat Gambuj Mosque,Sona Mosque,Shiva Temple,Bangram Rajbari'),
(48, 'Narail', 1984, 'Khulna', 990, 715000, 48.55, 'Kadamtala Mosque,Ghazir Dargah at Naldi,Lakshmipasha Kalibari'),
(49, 'Jessore', 1781, 'Khulna', 2567, 2742000, 51.28, 'Chanchara Rajbari,Kali Mandir,Dighi and Mandir at Siddirpasha'),
(50, 'Jhenaidah', 1984, 'Khulna', 1961, 1756000, 44.66, 'Harihar Garh,Shailkupa Jami Mosque,Ram Gopal Mandir'),
(51, 'Magura', 1984, 'Khulna', 1049, 913000, 44.7, 'Ghat of Nader Chand,Rajbari of Raja Satrujit Roy,'),
(52, 'Chuadanga', 1984, 'Khulna', 1177, 1123000, 40.88, 'Thakurpur Mosque,Hazar Duari School,Gholdari Neelkuthi'),
(53, 'Kushtia', 1947, 'Khulna', 1601, 1933000, 40.37, 'Rabindranath Tagore(Kuthibari),The Shrine of Lalon Fakir'),
(54, 'Meherpur', 1984, 'Khulna', 716, 652000, 37.79, 'Gosaidubi Mosque,Ballavpur Mission,Teragharia Marrut'),
(55, 'Barisal', 1797, 'Barisal', 2785, 2291000, 56.99, 'Shongrram Kella,Shorifal Dargah,Lonta Babur Dighi'),
(56, 'Jhalokati', 1984, 'Barisal', 749, 596000, 65.34, 'Nurollapur Math,Shujabad Kella,Civil Court Vaban'),
(57, 'Pirojpur', 1984, 'Barisal', 1308, 1103000, 64.31, 'Kabi Begum Sufia Kamal,Kabi Ahsan Habib,Leader Mahiuddin Ahmed'),
(58, 'Barguna', 1984, 'Barisal', 1831, 882000, 55.28, 'Betagir Bibicini Mosque,Taltoli Buddha Mondir'),
(59, 'Patuakhali', 1969, 'Barisal', 3221, 1517000, 51.65, 'Kalaia Bandar,Mohipur,PUST'),
(60, 'Bhola', 1984, 'Barisal', 3403, 1758000, 36.88, 'Kutuba Mia Bari,Monpura,Gazaria Farazi Bari'),
(61, 'Sylhet', 1782, 'Sylhet', 3490, 3404000, 45.59, 'Hazrat Shah Jalal and Shah Paran Tomb,Jaflong,Sust'),
(62, 'Moulvibazar', 1984, 'Sylhet', 2799, 1902000, 42.06, 'Madhobkundo Water Fall,Madhob Mondir,Nirmay Shib Bari'),
(63, 'Habiganj', 1984, 'Sylhet', 2637, 2059000, 37.72, 'Ranir Dighi,Tea Garden,Kalni River,Baniya Chor'),
(64, 'Sunamganj', 1984, 'Sylhet', 3670, 2443000, 34.37, 'Hayil Hawor,Bisho Juddho Shomadhi,Chatok Paper Mill');
